package main

import (
	"fmt"
	"gitlab.com/darwintradekz/core/performer/perfClient"
	"os"
)

func main() {
	settings := make(perfClient.Settings)
	//создадим настройки и укажем в них адрес performer-server, запущенного на прошлом шаге
	settings["test-account"] = perfClient.PerformerSettings{
		Name:     "test-performer",
		Host:     "localhost",
		Port:     "10655",
		CertPath: os.Getenv("HOME")+"/go/src/gitlab.com/darwintradekz/core/performer/performer.crt", //путь к только что созданному сертификату
	}

	performers, err := perfClient.NewPerformers(settings)
	if err != nil {
		panic(err)
	}

	//проверочный запрос на биржу BINANCE (see https://binance-docs.github.io/apidocs/spot/en/#test-connectivity)
	//указываем account="test-account", т.к. в настройках мы указали именно этот аккаунт (см код выше)
	err = performers.DoGet("test-account", "https://api.binance.com/api/v3/ping", false, nil)
	if err != nil {
		panic(err)
	}

	fmt.Println("it works!!!")
}
